from __future__ import division
import random

#functions
def get_all_loci(all_loc_file):
    l = 0
    for line in open(all_loc_file):
        l += 1
        if l == 2:
            line2 = line.rstrip("\n").split(",")
            all_loci = [int(lo.split("_")[0]) for lo in line2]
    return(all_loci)

def get_sel_loci(sel_loc_file):
    info_sel = {}
    for line in open(sel_loc_file):
        line2 = line.rstrip("\n").split("\t")
        info_sel[int(line2[0].split("_")[0])] = line2[1]
    return(info_sel)

def get_coding_info(coding_file):
    l = 0
    coding_info = {}
    for line in open(coding_file):
        l += 1
        if l > 1:
            line2 = line.rstrip("\n").split("\t")
            coding_info[int(line2[0])] = line2[1]
    return(coding_info)

def test_coding(locus_set,all_loci,permut,coding_info):
    total = len(locus_set)
    coding = [lo for lo in locus_set if lo in coding_info.keys()]
    rand = []
    for p in range(0,permut):
        rand.append(random.sample(all_loci,total))

    rand_coding = []
    for ra in rand:
        rand_coding.append(len([lo for lo in ra if lo in coding_info.keys()])/total)

    data_coding_comp = len(filter(lambda x: x >= len(coding)/total,rand_coding))
    return(data_coding_comp/permut)


#files
#genepop file with all loci
all_loc_file = "pop_new/b5_s7.pop"
#file including significant loci
sel_loc_file = "lfmm_qvalues/sig_Loci_min2_5-PCAdapt"
#file including functional data (blast)
coding_file = "blast/overview_b5_s7.tsv"

#number of permuations
permut = 1000

#load data
all_loci = get_all_loci(all_loc_file)
info_sel = get_sel_loci(sel_loc_file)
types = list(set(info_sel.values()))
coding_info = get_coding_info(coding_file)

#Calculate statistics
p_all_sel = test_coding(info_sel.keys(),all_loci,permut,coding_info)
print("P all selected: " + str(p_all_sel))

sel_min2_5 = [lo for lo,ty in info_sel.items() if ty == "Min2_5" or ty == "both"]
p_sel_min2_5 = test_coding(sel_min2_5,all_loci,permut,coding_info)
print("P Min2-5: " + str(p_sel_min2_5))

sel_PCA = [lo for lo,ty in info_sel.items() if ty == "PCAdapt" or ty == "both"]
p_sel_PCA = test_coding(sel_PCA,all_loci,permut,coding_info)
print("PCAdapt selected: " + str(p_sel_PCA))

sel_both = [lo for lo,ty in info_sel.items() if ty == "both"]
p_sel_both = test_coding(sel_both,all_loci,permut,coding_info)
print("both selected: " + str(p_sel_both))