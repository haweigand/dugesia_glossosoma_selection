log_files = snakemake.input
pairs = snakemake.params.pair
end = snakemake.params.end
out = snakemake.output[0]
folder= "lfmm/"

output = open(out, "w")
output = open(out, "a")
output.write("name\tmaf\tmds\tmdi\tmax_snps\tdist\tbatch\tInd\tSNMF")

for fi in log_files:
    fin = str(fi)
    name = fin[len(folder):fin.find(".log")]
    snmf_fi = folder + name + ".info_best_run"
    ind_fi = folder + name + ".lfmm_ind"
    ind_list = []
    val_dic = {}
    with open(snmf_fi) as file1:
        l = 0
        for line in file1:
            line2 = line.rstrip("\n").split("\t")
            if l == 0:
                k = line2[1]
            elif l == 1:
                r = line2[1]
            l += 1

    name = str(snmf_fi)[len(folder):str(snmf_fi).find(".info_best_run")]
    fi_name = name.split("_")
    snmf_file = (folder + name + ".snmf/K" + str(k) + "/run" + str(r) + "/" + name + "_r" + str(r) + "." + str(k) + ".Q")

    with open(ind_fi) as file2:
        for line in file2:
            ind_list.append(line.rstrip("\n"))
    i = 0

    with open(snmf_file) as file3:
        for line in file3:
            line2 = line.rstrip("\n").split(" ")
            val_dic[ind_list[i]] = line2
            i += 1

    for pa in pairs:
        sn1 = val_dic.get(pa + end[0],"No data")
        sn2 = val_dic.get(pa + end[1],"No data")
        output.write("\n" + fi_name[0] + "\t" + fi_name[1][3:] + "\t" + fi_name[2][3:] + "\t" + fi_name[3][3:] + "\t"+ fi_name[4][4:] + "\t"+ fi_name[5][4:] + "\t" + fi_name[6][1:] + "\t")
        if sn1 == "No data":
            output.write(pa + str(end[0]) + "\t" + "No data")
        else:
            output.write(pa + str(end[0]))
            for s in sn1:
                output.write("\t" + s)

        output.write("\n" + fi_name[0] + "\t" + fi_name[1][3:] + "\t" + fi_name[2][3:] + "\t" + fi_name[3][3:] + "\t"+ fi_name[4][4:] + "\t"+ fi_name[5][4:] + "\t" + fi_name[6][1:] + "\t")
        if sn2 == "No data":
            output.write( pa + str(end[1]) + "\t" + "No data")
        else:
            output.write(pa + str(end[1]))
            for s in sn2:
                output.write("\t" + s)

output.close()
