#!/usr/bin/env python
fasta_files = snakemake.input
pairs = snakemake.params.pair
end = snakemake.params.end
out = snakemake.output[0]
folder= "fasta/fd/"

output = open(out, "w")
output = open(out, "a")
output.write("name\tmaf\tmds\tmdi\tmax_snps\tdist\tbatch\tpair\ttotal\tdifferent\tNs")

for fa in fasta_files:
    i = 0
    seq = {}
    stat = {}
    fa_name = str(fa)
    fa_name2 = fa_name[len("fasta/"):fa.find(".fa")].split("_")

    for line in open(fa):
        i = i + 1
        if i % 2 == 1:
            name = line.rstrip("\n").replace(">","")
        else:
            seq[name] = line.rstrip("\n")

    for pa in pairs:
        s1 = seq.get(pa + end[0],"No seq")
        s2 = seq.get(pa + end[1],"No seq")
        total = len(s1)
        identical = 0
        ns = 0
        different = 0
        if s1 == "No seq" or s2 == "No seq":
            output.write("\n" + fa_name2[0] + "\t" + fa_name2[1][3:] + "\t" + fa_name2[2][3:] + "\t" + fa_name2[3][3:] + "\t"+ fa_name2[4][4:] + "\t"+ fa_name2[5][4:] + "\t" + fa_name2[6][1:] + "\t")
            output.write(pa + "\t" + "No sequences" + "\t\t\t\t")
        else:
            for p in range(0,len(s1)):
                base1 = s1[p]
                base2 = s2[p]
                if base1 == "N" or base2 == "N":
                    ns = ns + 1
                elif base1 == base2:
                    identical = identical + 1
                else:
                    different = different + 1
            total1 = ns + identical + different
            if total1 != total:
                output.write("\n" + fa_name2[0] + "\t" + fa_name2[1][3:] + "\t" + fa_name2[2][3:] + "\t" + fa_name2[3][3:] + "\t"+ fa_name2[4][4:] + "\t"+ fa_name2[5][4:] + "\t" + fa_name2[6][1:] + "\t")
                output.write(pa + "\t" + "Problems with sequences" + "\t\t\t\t")
            else:
                output.write("\n" + fa_name2[0] + "\t" + fa_name2[1][3:] + "\t" + fa_name2[2][3:] + "\t" + fa_name2[3][3:] + "\t"+ fa_name2[4][4:] + "\t"+ fa_name2[5][4:] + "\t" + fa_name2[6][1:] + "\t")
                output.write(pa + "\t" + str(total) + "\t" + str(different) + "\t" + str(ns))
