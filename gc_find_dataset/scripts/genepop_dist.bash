genepop=$(basename $1)
ca=$2
out=$3


#Get loci and SNP names
cat $1 | awk 'NR ==2' | sed 's/,/\n/g' | sed "s/_/\t/g" | sort > temp/fd/$genepop.snps
cat temp/fd/$genepop.snps | awk '{print $1}' | sort | uniq > temp/fd/$genepop.locus

#Get positions of loci
cat temp/fd/$genepop.locus | while read locus;
do
  cat $ca | awk -v lo=$locus '{OFS="\t"} {if ($1 == lo) print}' >> $out
done
