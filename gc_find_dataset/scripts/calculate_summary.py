#!/usr/bin/env python
#Generate empty variables
locs = []
inds = []
name = []

#get folder name
out = snakemake.output
out1 = str(out)
folder= out1[0:out1.find("statistics")]

#get statistics & file name
for in1 in snakemake.input:
    with open(in1) as file1:
        for line in file1:
            if line.startswith("Loci exported:"):
                loc = line.rstrip("\n").split(":")[1]
                locs.append(loc)
            if line.startswith("Individuals exported:"):
                ind = line.rstrip("\n").split(":")[1]
                inds.append(ind)
    name1 = str(in1)[len(folder):in1.find(".log")].split("_")
    name.append(name1)

#Generate output
output_s = open(out[0], "w")
output_s = open(out[0], "a")
if len(name[0]) == 7:
    output_s.write("name\tmaf\tmds\tmdi\tmax_snps\tdist\tbatch\tloci\tinds")
    for pos in range(len(name)):
        output_s.write("\n" + name[pos][0] + "\t" + name[pos][1][3:] + "\t" + name[pos][2][3:] + "\t" + name[pos][3][3:] + "\t"+ name[pos][4][4:] + "\t"+ name[pos][5][4:] + "\t" + name[pos][6][1:] + "\t")
        output_s.write(locs[pos] + "\t")
        output_s.write(inds[pos])

elif len(name[0]) == 6:
    output_s.write("name\tmaf\tmds\tmdi\tmax_snps\tbatch\tloci\tinds")
    for pos in range(len(name)):
        output_s.write("\n" + name[pos][0] + "\t" + name[pos][1][3:] + "\t" + name[pos][2][3:] + "\t" + name[pos][3][3:] + "\t"+ name[pos][4][4:] + "\t" + name[pos][5][1:] + "\t")
        output_s.write(locs[pos] + "\t")
        output_s.write(inds[pos])

elif len(name[0]) == 4:
    output_s.write("name\tmaf\tmds\tmdi\tloci\tinds")
    for pos in range(len(name)):
        output_s.write("\n" + name[pos][0] + "\t" + name[pos][1][3:] + "\t" + name[pos][2][3:] + "\t" + name[pos][3][3:] + "\t")
        output_s.write(locs[pos] + "\t")
        output_s.write(inds[pos])

output_s.close()
