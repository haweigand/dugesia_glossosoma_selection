inp_l=$1
genepop=$(basename $2)
dist=$3
out=$4
maf=$5
mds=$6
mdi=$7

#Use snps_location.py to generate Whitelist of accepted loci for modify_genepop.py
python scripts/snps_location.py $inp_l temp/fd/$genepop.snps temp/fd/$genepop.$3.wl -l $3

#Generate genepop file only with Whitelist loci
python scripts/modify_genepop.py -i $2 -w temp/fd/$genepop.$3.wl --maf $5 --mds $6 --mdi $7 -G $4
