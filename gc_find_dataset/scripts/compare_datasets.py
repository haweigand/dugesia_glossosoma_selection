#!/usr/bin/env python

fa_file = snakemake.input.fasta
pca_file = snakemake.input.pca
snmf_file = snakemake.input.snmf
pairs = snakemake.params.pair
end = snakemake.params.end
out = snakemake.output[0]

def calculate_fasta_stat(input_file):
    #Read data
    with open(input_file) as file1:
        names = []
        l = 0
        conflicts = {}
        for line in file1:
            if l > 0:
                line2 = line.rstrip("\n").split("\t")
                name = line2[0]
                for na in line2[1:7]:
                    name = name + "_" + na
                names.append(name)
                if line2[8] != "No sequences":
                    val = int(line2[9])/(int(line2[8])+int(line2[10]))
                    vals = conflicts.get(name,[])
                    vals.append(val)
                    conflicts[name] = vals
            l += 1

    #Calculate mean
    conflict_mean = {}
    for name, vals in conflicts.items():
        value = 0
        for val in vals:
            value += val
        conflict_mean[name] = value/len(vals)

    return(conflict_mean,names)

def calculate_pca_stat(input_file):
    #Read data
    with open(input_file) as file1:
        l = 0
        differences = {}
        for line in file1:
            if l > 0:
                line2 = line.rstrip("\n").split("\t")
                name = line2[0]
                for na in line2[1:7]:
                    name = name + "_" + na
                ind = line2[7]
                pc1 = line2[8]
                vals = differences.get(name,{})
                vals[ind] = pc1
                differences[name] = vals
            l += 1

    #Calculate mean
    differences_mean = {}
    for name,values in differences.items():
        vals = 0
        total = 0
        for pa in pairs:
            v1 = values.get(pa+end[0])
            v2 = values.get(pa+end[1])
            if v1 != "No data" and v2 != "No data":
                total += (abs(float(v1) - float(v2)))
                vals += 1
        if vals != 0:
            differences_mean[name] = total/vals

    return(differences_mean)

def calculate_snmf_stat(input_file):
    #Read data
    with open(input_file) as file1:
        l = 0
        differences = {}
        for line in file1:
            if l > 0:
                line2 = line.rstrip("\n").split("\t")
                name = line2[0]
                for na in line2[1:7]:
                    name = name + "_" + na
                ind = line2[7]
                snmf = line2[8:]
                vals = differences.get(name,{})
                vals[ind] = snmf
                differences[name] = vals
            l += 1

    #Calculate mean
    differences_mean = {}
    for name,values in differences.items():
        value = 0
        total = 0
        for pa in pairs:
            v1 = values.get(pa+end[0])
            v2 = values.get(pa+end[1])
            if v1 != ["No data"] and v2 != ["No data"]:
                for pos in range(len(v1)-1):
                    total += (abs(float(v1[pos]) - float(v2[pos])))
                    value += 1
        if value != 0:
            differences_mean[name] = total

    return(differences_mean)

fasta,names = calculate_fasta_stat(fa_file)
pca = calculate_pca_stat(pca_file)
snmf = calculate_snmf_stat(snmf_file)

output = open(out, "w")
output = open(out, "a")
output.write("name\tmaf\tmds\tmdi\tmax_snps\tdist\tbatch\tFasta\tPCA\tSNMV")

names = list(set(names))

for name in names:
    fa = fasta.get(name , "no data")
    pc = pca.get(name, "no data")
    sn = snmf.get(name, "no data")
    na = name.split("_")
    output.write("\n" + na[0] + "\t" + na[1] + "\t" + na[2] + "\t" + na[3] + "\t" + na[4] + "\t" + na[5] + "\t" + na[6])
    output.write("\t" + str(fa) + "\t" + str(pc) + "\t" + str(sn))

output.close()
