import os

configfile: "snp_test.yaml"

#Say which files have to ge generated
rule all:
    input:
        "logs/ff/statistics",
        "logs/fns/statistics",
        "logs/fd/statistics",
        "logs/fd/ref/statistics_fa",
        "logs/fd/fa/confic.txt",
        "logs/fd/lfmm/pca_statistics",
        "logs/fd/lfmm/snmf_statistics",
	    "logs/statistics_comparison.txt",
        expand("fd/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.pop", file=config["files"], maf=config["maf"], mds=config["mds"], mdi=config["mdi"],snps=config["snps"],dist=config["dist"],cat=config["cat"]),
        expand("fasta/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.fa", file=config["files"], maf=config["maf"], mds=config["mds"], mdi=config["mdi"],snps=config["snps"],dist=config["dist"],cat=config["cat"]),
        expand("lfmm/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.log", file=config["files"], maf=config["maf"], mds=config["mds"], mdi=config["mdi"],snps=config["snps"],dist=config["dist"],cat=config["cat"])
#Run filtering for the first time
rule filter_first_time:
    input:
        "pop/{file}.pop",
    output:
        o1="ff/{file}_maf{maf}_mds{mds}_mdi{mdi}.pop",
        o2="logs/ff/{file}_maf{maf}_mds{mds}_mdi{mdi}.log"
    shell:
        "python3 scripts/modify_genepop.py -i {input} --mdi {wildcards.mdi} --maf {wildcards.maf} --mds {wildcards.mds} -G {output.o1} > {output.o2}"

#Prepare catalog file for number of SNP filtering
rule prepare_catalog_snps1:
    input:
        expand("batch_{cat}.catalog.snps.tsv.gz", cat=config["cat"])
    output:
        "temp/fns/b{cat}.snpcount"
    shell:
        "gzip -cd {input} | awk '{{print $3}}' | sort | uniq -c > {output}"

rule prepare_catalog_snps2:
    input:
        "temp/fns/b{cat}.snpcount"
    output:
        "temp/fns/b{cat}.loci.1_{snps}"
    shell:
        "cat {input} | awk -v max={wildcards.snps} '{{if ($1 >= 1 && $1 <= max) print $2}}' | sort > {output}"

#Filter by number of SNPs
rule filter_by_number_of_snps:
    input:
        fi="ff/{file}_maf{maf}_mds{mds}_mdi{mdi}.pop",
        ca="temp/fns/b{cat}.loci.1_{snps}"
    output:
        o1="fns/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_b{cat}.pop",
        o2="logs/fns/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_b{cat}.log"
    run:
        shell("bash scripts/genepop_nos.bash {input.fi} {input.ca} {wildcards.snps} {output.o1} {wildcards.maf} {wildcards.mds} {wildcards.mdi} > {output.o2} ")

#Prepare catalog file for distance filtering
rule prepare_catalog_dist1:
    input:
        "batch_{cat}.catalog.tags.tsv.gz"
    output:
        "temp/fd/b{cat}.positions"
    shell:
        "gzip -cd {input} | awk '{{print $3, $4, $5, $6, $10}}' | tr ' ' '\t' > {output}"

rule filter_by_distance1:
    input:
        inp_p="fns/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_b{cat}.pop",
        inp_l="temp/fd/b{cat}.positions"
    output:
        "temp/fd/b{cat}.{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}.pop.locus.info"
    run:
        shell("bash scripts/genepop_dist.bash {input.inp_p} {input.inp_l} {output}")

rule filter_by_distance2:
    input:
        inp_l="temp/fd/b{cat}.{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}.pop.locus.info",
        pop="fns/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_b{cat}.pop"
    output:
        o1="fd/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.pop",
        o2="logs/fd/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.log"
    run:
        shell("bash scripts/genepop_dist2.bash {input.inp_l} {input.pop} {wildcards.dist} {output.o1} {wildcards.maf} {wildcards.mds} {wildcards.mdi} > {output.o2} ")

rule summary_ff:
    input:
        expand("logs/ff/{file}_maf{maf}_mds{mds}_mdi{mdi}.log", file=config["files"], maf=config["maf"], mds=config["mds"], mdi=config["mdi"], cat=config["cat"])
    output:
        "logs/ff/statistics"
    script:
        "scripts/calculate_summary.py"

rule summary_fns:
    input:
        expand("logs/fns/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_b{cat}.log", file=config["files"], maf=config["maf"], mds=config["mds"], mdi=config["mdi"],snps=config["snps"],cat=config["cat"])
    output:
        "logs/fns/statistics"
    script:
        "scripts/calculate_summary.py"

rule summary_fd:
    input:
        expand("logs/fd/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.log", file=config["files"], maf=config["maf"], mds=config["mds"], mdi=config["mdi"],snps=config["snps"], dist=config["dist"],cat=config["cat"])
    output:
        "logs/fd/statistics"
    script:
        "scripts/calculate_summary.py"

rule get_fasta_lfmm:
    input:
        "fd/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.pop"
    output:
        out1="fasta/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.fa",
        out2="lfmm/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.lfmm",
        out3="logs/fd/ref/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.log"
    shell:
        "python3 scripts/modify_genepop.py -i {input} --mdi {wildcards.mdi} --maf {wildcards.maf} --mds {wildcards.mds} -F {output.out1} -L {output.out2} > {output.out3}"

rule summary_fa_lfmm:
    input:
        expand("logs/fd/ref/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.log", file=config["files"], maf=config["maf"], mds=config["mds"], mdi=config["mdi"],snps=config["snps"], dist=config["dist"],cat=config["cat"])
    output:
        o1="logs/fd/ref/statistics_fa",
        o2="logs/fd/ref/statistics_lfmm"
    script:
        "scripts/calculate_summary2.py"

rule fa_conflict:
    input:
        expand("fasta/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.fa", file=config["files"], maf=config["maf"], mds=config["mds"], mdi=config["mdi"],snps=config["snps"], dist=config["dist"],cat=config["cat"])
    params:
        pair=config["pairs"],
        end=config["ending"]
    output:
        "logs/fd/fa/confic.txt"
    script:
        "scripts/fa_conflict.py"

rule calculate_pca_snmf:
    input:
        "lfmm/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.lfmm"
    output:
        "lfmm/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.log"
    shell:
        "Rscript scripts/lea_pca_snmf.R -f {input} -o 'lfmm/{wildcards.file}_maf{wildcards.maf}_mds{wildcards.mds}_mdi{wildcards.mdi}_snps{wildcards.snps}_dist{wildcards.dist}_b{wildcards.cat}' > {output}"

rule compare_pca:
    input:
    #Hier ändern auch in compare_snmf
        expand("lfmm/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.log", file=config["files"], maf=config["maf"], mds=config["mds"], mdi=config["mdi"],snps=config["snps"], dist=config["dist"],cat=config["cat"])
    output:
        "logs/fd/lfmm/pca_statistics"
    params:
        pair=config["pairs"],
        end=config["ending"]
    script:
        "scripts/compare_pca.py"

rule compare_snmf:
    input:
        expand("lfmm/{file}_maf{maf}_mds{mds}_mdi{mdi}_snps{snps}_dist{dist}_b{cat}.log", file=config["files"], maf=config["maf"], mds=config["mds"], mdi=config["mdi"],snps=config["snps"], dist=config["dist"],cat=config["cat"])
    output:
        "logs/fd/lfmm/snmf_statistics"
    params:
        pair=config["pairs"],
        end=config["ending"]
    script:
        "scripts/compare_snmf.py"

rule compare_datasets:
    input:
        fasta="logs/fd/fa/confic.txt",
        pca="logs/fd/lfmm/pca_statistics",
        snmf="logs/fd/lfmm/snmf_statistics"
    output:
        "logs/statistics_comparison.txt"
    params:
        pair=config["pairs"],
        end=config["ending"]
    script:
        "scripts/compare_datasets.py"
