library(pcadapt)
#source("https://bioconductor.org/biocLite.R")
#biocLite("qvalue")
library(qvalue)

setwd("lfmm_new")

#read data
data = read.pcadapt("lfmm/b5_s7.lfmm", type="lfmm")

#test number of axis
pc1 = pcadapt(data, K=15, output.filename="Dg_test_pcadapt")

#produce screeplots to view number of axis
plot(pc1, option="screeplot")

#K3,11
#recalculate test Dg
pc3 = pcadapt(data, K=3, output.filename="Dg_pcadapt_K3")
name3 = "pcadapt/pca_b5_s7_K3.txt"

pc11 = pcadapt(data, K=11, output.filename="Dg_pcadapt_K11")
name11 = "pcadapt/pca_b5_s7_K11.txt"

#visualise results
#qqplot von p-values
plot(pc3, option="qqplot")
plot(pc11, option="qqplot")
#Value distribution & Neutral
plot(pc3, option="stat.distribution")
plot(pc11, option="stat.distribution")
#Genomic distribution of values
plot(pc3, option="manhattan")
plot(pc11, option="manhattan")


#calculation of qvalues
pval3 = pc3$pvalues
qobj3 = qvalue(p = pval3)

#Schauen ob q-values so sinnvoll sind
hist(pval3, nclass=20)

#Verteilung der Werte anschauen
summary(qobj3)
hist(qobj3)
plot(qobj3)

#pvalues output
a = do.call(rbind, Map(data.frame, A=qobj3$pvalues, B=qobj3$qvalues))
write.table(a, name3, row.names = F)

#calculation of qvalues
pval11 = pc11$pvalues
qobj11 = qvalue(p = pval11)

#Schauen ob q-values so sinnvoll sind
hist(pval11, nclass=20)

#Verteilung der Werte anschauen
summary(qobj11)
hist(qobj11)
plot(qobj11)

#pvalues output
a = do.call(rbind, Map(data.frame, A=qobj11$pvalues, B=qobj11$qvalues))
write.table(a, name11, row.names = F)