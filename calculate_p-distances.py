from __future__ import division
import random
import os



#functions used for calculation

def fill_dic_list(dic,entry,value):
    old = dic.get(entry,[])
    old.append(value)
    dic[entry] = old
    return(dic)

def selected_loci_info(file1):
    loci = []
    info = {}
    info2 = {}
    for line in open(file1):
        line2 = line.rstrip("\n").split("\t")
        if len(line2) > 1:
            loci.append(line2[0])
            info[line2[0]] = line2[1]
            info2 = fill_dic_list(info2,line2[1],line2[0])
    loci2 = [va for val in info2.values() for va in sorted(val) ]
    return(loci2,info)

def pop_assignment(pop_ass_file):
    pop_dic = {}
    for line in open(pop_ass_file):
        line2 = line.rstrip("\n").split("\t")
        if len(line2) == 2:
            pop = line2[1]
            ind = line2[0]
            pop_dic = fill_dic_list(pop_dic,pop,ind)
    return(pop_dic)

def read_pop_file(pop_file,pop_dic):
    loc_data = {}
    l = 0
    for line in open(pop_file):
        l += 1
        if l == 2:
            line2 = line.rstrip("\n").split(",")
            loc_list = line2

        if l > 2:
            line2 = line.rstrip("\n").replace(" ","").replace(" ","").replace(",","\t").split("\t")
            if len(line2) > 1:
                ind = line2[0]
                locs = line2[1:]
                if locs[0] == "":
                    locs = locs[1:]
                for lo in range(0,len(loc_list)):
                    old = loc_data.get(loc_list[lo],{})
                    old[ind] = locs[lo]
                    loc_data[loc_list[lo]] = old

    return(loc_data)

def calc_sumit(s1,s2):
    sumit = 0
    if s1 == s2:
        sumit +=1
    else:
        sc1 = 0
        sc2 = 0
        if str(s1[1]) == str(s2[1]) or str(s1[1]) == str(s2[3]):
            sc1 += 1
        if str(s1[3]) == str(s2[1]) or str(s1[3]) == str(s2[3]):
            sc1 += 1
        if str(s2[1]) == str(s1[1]) or str(s2[1]) == str(s1[3]):
            sc2 += 1
        if str(s2[3]) == str(s1[1]) or str(s2[3]) == str(s1[3]):
            sc2 += 1
        sc = (sc1/2) * (sc2/2)
        sumit += sc
    return(sumit)

def sumit_matrix():
    pos = ["01","02","03","04"]
    nuc = []
    s_matrix ={}
    for p1 in pos:
        for p2 in pos:
            nuc.append(p1+p2)
    for nu1 in nuc:
        for nu2 in nuc:
            sumit = calc_sumit(nu1,nu2)
            s_matrix[nu1+nu2] = sumit
    return(s_matrix)

def calc_pdist(seq1,seq2,s_matrix):
    sumit = 0
    length = 0
    for nu in range(0,len(seq1)):
        if seq1[nu] != "0000" and seq2[nu] != "0000":
            length += 1
            sumit = sumit + s_matrix.get(seq1[nu]+seq2[nu])
    if length != 0:
        score = sumit/length
        pdist = 1-score
    else:
        pdist = "NA"
    return(pdist)


def mean(list1):
    total = sum(list1)
    return(total/len(list1))

def calc_pdist_all_pops(pop_order,loci,data_loc):
    pdist_dic = {}
    pop1_done = []
    for pop1 in pop_order:
        pop1_done.append(pop1)
        for pop2 in pop_order:
            if (pop2 in pop1_done) == False:
                pdists = []
                inds1 = pop_dic.get(pop1)
                inds2 = pop_dic.get(pop2)
                for in1 in inds1:
                    for in2 in inds2:
                        seq1 = []
                        seq2 = []
                        for lo in loci:
                            seq1.append(data_loc.get(lo).get(in1))
                            seq2.append(data_loc.get(lo).get(in2))
                        pdists.append(calc_pdist(seq1,seq2,sumit_matrix()))
                pdists = [pd for pd in pdists if pd != "NA"]
                pdist_dic[pop1 + "-" + pop2] = mean(pdists)
    return(pdist_dic)

def calc_random_pdis(data_loc,loci,pop_order,permut):
    rand_dic = {}
    for p in range(0,permut):
        locs = random.sample(data_loc.keys(),len(loci))
        rand = calc_pdist_all_pops(pop_order,locs,data_loc)
        for pop,val in rand.items():
            rand_dic = fill_dic_list(rand_dic,pop,val)
    return(rand_dic)

def calc_pvalues(sel_pdist,rand_pdist):
    pvals = {}
    for comp,vals in rand_pdist.items():
        sel_val = sel_pdist.get(comp)
        pvals[comp] = get_pval(vals,sel_val)
    return(pvals)

def write_output(output,sel_pdist,rand_pdist,pvals_pdist):
    out = open(output,"w")
    out.write("Pop1\tPop2\tpdist\tr-mean\tp-value")
    for pops,vals in sel_pdist.items():
        out.write("\n" + pops.split("-")[0] + "\t" + pops.split("-")[1] + "\t" + str(vals) + "\t" + str(mean(rand_pdist.get(pops))) + "\t" + str(pvals_pdist.get(pops)))
    out.close()


#used files
#list of included loci
loc_list = "lfmm_qvalues/sig_Loci_min2_5-PCAdapt"
#genpop file including all individuals and loci
pop_file = "pop_new/b5_s7.pop"
#population assignment file for all individuals used to calculate p-distances
pop_ass_file = "all_freq_test/pop_ass2"
#order of the populations for the output
pop_order=["HSK6","HSK9","HSK5","HSK10","OE5","KL3","KL6","KL9","KL13","VR11","VR17"]
#output file
out = "pdist/test_pdist"


#number of permuations
permut = 1000

# load data
loci,info = selected_loci_info(loc_list)
pop_dic = pop_assignment(pop_ass_file)
data_loc = read_pop_file(pop_file,pop_dic)

#used loci for p-distance calculation
# all loci either found by pcadapt or correlated to Min2_5 in the lfmm analysis
sel_all = info.keys()
# all loci correlated to Min2_5 in the lfmm analysis
sel_min2_5 = [lo for lo,ty in info.items() if ty == "Min2_5" or ty == "both"]
# all loci found by pcadapt
sel_PCA = [lo for lo,ty in info.items() if ty == "PCAdapt" or ty == "both"]
# all loci which were found by pcadapt and correlated to Min2_5 in the lfmm analysis
sel_both = [lo for lo,ty in info.items() if ty == "both"]



sel_pdist = calc_pdist_all_pops(pop_order,sel_min2_5,data_loc)
rand_pdist =  calc_random_pdis(data_loc,sel_min2_5,pop_order,permut)
pvals_pdist = calc_pvalues(sel_pdist,rand_pdist)
output = "pdist/sel_min2_5_pdist"
write_output(output,sel_pdist,rand_pdist,pvals_pdist)
print("sel_min2_5 finished")


sel_pdist = calc_pdist_all_pops(pop_order,sel_PCA,data_loc)
rand_pdist =  calc_random_pdis(data_loc,sel_PCA,pop_order,permut)
pvals_pdist = calc_pvalues(sel_pdist,rand_pdist)
output = "pdist/sel_PCA_pdist"
write_output(output,sel_pdist,rand_pdist,pvals_pdist)
print("sel_PCA finished")


sel_pdist = calc_pdist_all_pops(pop_order,sel_both,data_loc)
rand_pdist =  calc_random_pdis(data_loc,sel_both,pop_order,permut)
pvals_pdist = calc_pvalues(sel_pdist,rand_pdist)
output = "pdist/sel_both_pdist"
write_output(output,sel_pdist,rand_pdist,pvals_pdist)
print("sel_both finished")


sel_pdist = calc_pdist_all_pops(pop_order,sel_all,data_loc)
rand_pdist =  calc_random_pdis(data_loc,sel_all,pop_order,permut)
pvals_pdist = calc_pvalues(sel_pdist,rand_pdist)
output = "pdist/sel_all_pdist"
write_output(output,sel_pdist,rand_pdist,pvals_pdist)
print("sel_all finished")
