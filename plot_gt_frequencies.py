from __future__ import division
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np

def fill_dic_list(dic,entry,value):
    old = dic.get(entry,[])
    old.append(value)
    dic[entry] = old
    return(dic)

def fill_dic_number(dic,entry,value):
    old = dic.get(entry,0)
    old += value
    dic[entry] = old
    return(dic)


def selected_loci_info(file1):
    loci = []
    info = {}
    info2 = {}
    for line in open(file1):
        line2 = line.rstrip("\n").split("\t")
        if len(line2) > 1:
            loci.append(line2[0])
            info[line2[0]] = line2[1]
            info2 = fill_dic_list(info2,line2[1],line2[0])
    loci2 = [va for val in info2.values() for va in sorted(val) ]
    return(loci2,info)



def pop_assignment(pop_ass_file):
    pop_dic = {}
    for line in open(pop_ass_file):
        line2 = line.rstrip("\n").split("\t")
        if len(line2) == 2:
            pop = line2[1]
            ind = line2[0]
            pop_dic = fill_dic_list(pop_dic,pop,ind)
    return(pop_dic)


def subplot_data(sub_data):
    AA = []
    Aa = []
    aa = []
    AA_Aa = []
    for x in sub_data:
        AA.append(x[0])
        Aa.append(x[1])
        AA_Aa.append(x[0]+x[1])
        aa.append(x[2])
    return(AA,Aa,aa,AA_Aa)

def plot_data(data,names,loci,info,name,file,):
    fig = plt.figure(figsize=(25,25))
    for i in range(1,len(data)+1):
        AA,Aa,aa,AA_Aa = subplot_data(data[i-1])
        n = len(AA)
        ax = plt.subplot(len(data)+1,1,i)
        width = 1
        bar_locations = np.array(range(0,n))
        ax.bar(bar_locations,AA,color = "blue",width=width)
        ax.bar(bar_locations,Aa,bottom=AA, color ="green",width=width)
        ax.bar(bar_locations,aa,bottom=AA_Aa, color = "yellow",width=width)
        plt.xlim([bar_locations[0],bar_locations[-1]+1])
        plt.ylabel(names[i-1],fontsize=12)
        plt.yticks([])
        if i < len(data):
            vis = False
        else:
            vis = True
        plt.xticks([b + 0.5 for b in bar_locations],[info.get(l) + ": " + l for l in loci],rotation=90,visible=vis)
    plt.savefig(file)

def read_pop_file(pop_file,loci,pop_dic):
    pop_data = {}
    l = 0
    for line in open(pop_file):
        l += 1
        if l == 2:
            line2 = line.rstrip("\n").split(",")
            loc_list = line2
            loc_list2 = [0 for li in loci]
            for loc in range(0,len(loc_list)):
                if loc_list[loc] in loci:
                    loc_list2[loci.index(loc_list[loc])] = loc

        if l > 2:
            line2 = line.rstrip("\n").replace(" ","").replace(" ","").replace(",","\t").split("\t")
            if len(line2) > 1:
                ind = line2[0]
                locs = line2[1:]
                if locs[0] == "":
                    print("!!!")
                    locs = locs[1:]
                for pop,inds in pop_dic.items():
                    if ind in inds:
                        pop_ind = pop
                        sel_loci = map(lambda x: locs[x],loc_list2)
                        pop_data = fill_dic_list(pop_data,pop_ind,sel_loci)

    return(pop_data)

def set_ref_allele(pop_data_inds,loci,pop_order):
    ref_alleles = {}
    for loc in range(0,len(loci)):
        ref_allele = "no"
        for pop in pop_order:
            if ref_allele == "no":
                data = pop_data_inds.get(pop)
                ac = {}
                maxa = ""
                maxc = 0
                for ind in data:
                    af1 = ind[loc][0:2]
                    af2 = ind[loc][2:]
                    if af1 != "00":
                        ac = fill_dic_number(ac,af1,1)
                        ac = fill_dic_number(ac,af2,1)
                for a,c in ac.items():
                    if c > maxc:
                        maxa = a
                        maxc = c
                if maxc != 0:
                    ref_allele = maxa
        ref_alleles[loci[loc]] = ref_allele
    return(ref_alleles)

def get_data_genotype_fre(pop_data_inds,loci,ref_alleles,pop_order):
    all_pop_data = []
    for pop in pop_order:
        ind_locs = pop_data_inds.get(pop)
        pop_data = []
        # genotypes per pop
        for loc in range(0,len(loci)):
            loc_name = str(loci[loc])
            genotypes = {}
            alleles = []
            for ind in ind_locs:
                gt = ind[loc]
                if gt != "0000":
                    genotypes = fill_dic_number(genotypes,gt,1)
                    a1 = gt[0:2]
                    a2 = gt[2:]
                    alleles.append(a1)
                    alleles.append(a2)
            alleles = list(set(alleles))
            if len(alleles) > 2:
                print("more than two alleles for locus " + loc_name)
            elif len(alleles) == 1:
                ref_all = ref_alleles.get(loc_name)
                if alleles[0] == ref_all:
                    alt_all = "55"
                else:
                    alt_all = alleles[0]
            elif len(alleles) == 2:
                ref_all = ref_alleles.get(loc_name)
                if ref_all in alleles:
                    alt_all = alleles[1-alleles.index(ref_all)]
                else:
                    print("more than two alleles at loc " + + loc_name)
            AA_c = genotypes.get(ref_all+ref_all,0)
            aa_c = genotypes.get(alt_all+alt_all,0)
            Aa_c1 = genotypes.get(ref_all+alt_all,0)
            Aa_c2 = genotypes.get(alt_all+ref_all,0)
            total = AA_c + aa_c + Aa_c1 + Aa_c2
            if total == 0:
                pop_data.append([0,0,0])
                print(" ".join(["no data",pop,loc_name]))
            else:
                pop_data.append([AA_c/total,(Aa_c1+Aa_c2)/total,aa_c/total])
        all_pop_data.append(pop_data)
    return(all_pop_data)

#load data

#list of loci which should be plotted
loc_list = "../lfmm_qvalues/sig_Loci_min2_5-PCAdapt"

#genepop file including these loci and the populations which should be plotted
pop_file = "../pop_new/b5_s7.pop"

#file stating which individuals belong to which population
pop_ass_file = "../all_freq_test/pop_ass"

#order of the populations in the plot
pop_order=["HSK5_Min","HSK5_ref","HSK6_Min","HSK6_ref","HSK9_Min","HSK9_ref","HSK10_Min","HSK10_ref","OE5_Min","KL3_WWTP","KL3_ref","KL6_WWTP","KL6_ref","KL9_WWTP","KL9_ref","KL13_WWTP","KL13_ref","VR11_ref","VR17_ref"]

#name of the plot
name = "Min2_5-PCAdapt"

#all loci
loci,info = selected_loci_info(loc_list)
pop_dic = pop_assignment(pop_ass_file)
pop_data_inds = read_pop_file(pop_file, loci,pop_dic)

ref_alleles = set_ref_allele(pop_data_inds,loci,pop_order)
all_pop_data = get_data_genotype_fre(pop_data_inds,loci,ref_alleles,pop_order)
plot_data(all_pop_data,pop_order,loci,info,name,"name + ".svg")
