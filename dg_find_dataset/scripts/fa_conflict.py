#!/usr/bin/env python
fasta_files = snakemake.input
pairs = snakemake.params.pair
end = snakemake.params.end
out = snakemake.output[0]
folder= "fasta/fd/"

output = open(out, "w")
output = open(out, "a")
output.write("name\tbatch\tcover\tsnps\tmaf\tmds\tmdi\tpair\ttotal\tdifferent\tNs")

for fa in fasta_files:
    i = 0
    seq = {}
    stat = {}
    fa_name = str(fa)
    fi_name = fa_name[len("fasta/"):fa.find(".fa")].split("_")

    for line in open(fa):
        i = i + 1
        if i % 2 == 1:
            name = line.rstrip("\n").replace(">","")
        else:
            seq[name] = line.rstrip("\n")

    for pa in pairs:
        s1 = seq.get(pa + end[0],"No seq")
        s2 = seq.get(pa + end[1],"No seq")
        total = len(s1)
        identical = 0
        ns = 0
        different = 0
        if s1 == "No seq" or s2 == "No seq":
            output.write("\n" + fi_name[0] + fi_name[1] + "\t" + fi_name[2][1:] + "\t" + fi_name[3][1:] + "\t" + fi_name[4][1:] + "\t"+ fi_name[5][3:] + "\t"+ fi_name[6][3:] + "\t" + fi_name[7][3:] + "\t")
            output.write(pa + "\t" + "No sequences" + "\t\t\t\t")
        else:
            for p in range(0,len(s1)):
                base1 = s1[p]
                base2 = s2[p]
                if base1 == "N" or base2 == "N":
                    ns = ns + 1
                elif base1 == base2:
                    identical = identical + 1
                else:
                    different = different + 1
            total1 = ns + identical + different
            if total1 != total:
                output.write("\n" + fi_name[0] + "_" + fi_name[1] + "\t" + fi_name[2][1:] + "\t" + fi_name[3][1:] + "\t" + fi_name[4][1:] + "\t"+ fi_name[5][3:] + "\t"+ fi_name[6][3:] + "\t" + fi_name[7][3:] + "\t")
                output.write(pa + "\t" + "Problems with sequences" + "\t\t\t\t")
            else:
                output.write("\n" + fi_name[0] + "_" + fi_name[1] + "\t" + fi_name[2][1:] + "\t" + fi_name[3][1:] + "\t" + fi_name[4][1:] + "\t"+ fi_name[5][3:] + "\t"+ fi_name[6][3:] + "\t" + fi_name[7][3:] + "\t")
                output.write(pa + "\t" + str(total) + "\t" + str(different) + "\t" + str(ns))
