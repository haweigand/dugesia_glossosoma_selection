#!/usr/bin/env python
log_files = snakemake.input
pairs = snakemake.params.pair
end = snakemake.params.end
out = snakemake.output[0]
folder= "lfmm/"


output = open(out, "w")
output = open(out, "a")
output.write("name\tbatch\tcover\tsnps\tmaf\tmds\tmdi\tInd\tPC1\tPC2\tPC3")

for fi in log_files:
    fin = str(fi)
    name = fin[len(folder):fin.find(".log")]
    pca_fi = folder + name + ".pca/" + name + ".projections"
    ind_fi = folder + name + ".lfmm_ind"
    fi_name = name.split("_")
    ind_list = []
    val_dic = {}
    with open(ind_fi) as file1:
        for line in file1:
            ind_list.append(line.rstrip("\n"))
    l = 0
    with open(pca_fi) as file2:
        for line in file2:
            line2 = line.rstrip("\n").split(" ")
            ind = ind_list[l]
            val_dic[ind] = line2
            l += 1

    for pa in pairs:
        pcs1 = val_dic.get(pa + end[0],"No data")
        pcs2 = val_dic.get(pa + end[1],"No data")
        output.write("\n" + fi_name[0] + "_" + fi_name[1] + "\t" + fi_name[2][1:] + "\t" + fi_name[3][1:] + "\t" + fi_name[4][1:] + "\t"+ fi_name[5][3:] + "\t"+ fi_name[6][3:] + "\t" + fi_name[7][3:] + "\t")
        if pcs1 == "No data":
            output.write(pa + str(end[0]) + "\t" + "No data" + "\t\t")
        else:
            output.write(pa + str(end[0]) + "\t" + pcs1[0] + "\t" + pcs1[1] + "\t" + pcs1[2])
        output.write("\n" + fi_name[0] +  "_" + fi_name[1] + "\t" + fi_name[2][1:] + "\t" + fi_name[3][1:] + "\t" + fi_name[4][1:] + "\t"+ fi_name[5][3:] + "\t"+ fi_name[6][3:] + "\t" + fi_name[7][3:] + "\t")
        if pcs2 == "No data":
            output.write( pa + str(end[1]) + "\t" + "No data" + "\t\t")
        else:
            output.write( pa + str(end[1]) + "\t" + pcs2[0] + "\t" + pcs2[1] + "\t" + pcs2[2])

output.close()
