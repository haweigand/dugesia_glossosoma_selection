#!/usr/bin/env python
#Generate empty variables
locs_pop = []
locs_lfmm = []
inds_pop = []
inds_lfmm = []
name = []

#get folder name
out_pop = str(snakemake.output.o1)
out_lfmm = str(snakemake.output.o2)
folder = out_pop[0:out_pop.find("statistics")]

#get statistics & file name
for in1 in snakemake.input:
    with open(in1) as file1:
        typ="no"
        l = 0
        p = 0
        q = 0
        for line in file1:
            if line.startswith("Genepop output"):
                typ="pop"
                p = 1
                q = 1
            elif line.startswith("lfmm output"):
                typ="lf"
                p = 1
                q = 1
            elif line.startswith("Loci exported:"):
                loc = line.rstrip("\n").split(":")[1]
                if typ == "pop" and p == 1:
                    locs_pop.append(loc)
                elif typ == "lf" and p == 1:
                    locs_lfmm.append(loc)
                p += 1
            if line.startswith("Individuals exported:"):
                ind = line.rstrip("\n").split(":")[1]
                if typ == "pop" and q == 1:
                    inds_pop.append(ind)
                elif typ == "lf" and q == 1:
                    inds_lfmm.append(ind)
                q += 1
            l += 1
        if l == 1:
            inds_pop.append("ND")
            locs_pop.append("ND")
            inds_lfmm.append("ND")
            locs_lfmm.append("ND")
    name1 = str(in1)[len(folder):in1.find(".log")].split("_")
    name.append(name1)
    if p == 0 and l != 1:
        inds_pop.append("ND")
        locs_pop.append("ND")
        inds_lfmm.append("ND")
        locs_lfmm.append("ND")

#Generate output
output_pop = open(out_pop, "w")
output_pop = open(out_pop, "a")
output_pop.write("name\tbatch\tcov\tmax_snps\tmaf\tmds\tmdi\tloci\tinds")
for pos in range(len(name)):
    output_pop.write("\n" + str(name[pos][0]) + "_" + str(name[pos][1])+ "\t" + str(name[pos][2][1:]) + "\t" + str(name[pos][3][1:]) + "\t" + str(name[pos][4][1:]) + "\t"+ str(name[pos][5][3:]) + "\t"+ str(name[pos][6][3:]) + "\t" + str(name[pos][7][3:]) + "\t")
    output_pop.write(str(locs_pop[pos]) + "\t")
    output_pop.write(str(inds_pop[pos]))
output_pop.close()

output_lfmm = open(out_lfmm, "w")
output_lfmm = open(out_lfmm, "a")
output_lfmm.write("name\tbatch\tcov\tmax_snps\tmaf\tmds\tmdi\tloci\tinds")
for pos in range(len(name)):
    output_lfmm.write("\n" + str(name[pos][0]) + "_" + str(name[pos][1])+ "\t" + str(name[pos][2][1:]) + "\t" + str(name[pos][3][1:]) + "\t" + str(name[pos][4][1:]) + "\t"+ str(name[pos][5][3:]) + "\t"+ str(name[pos][6][3:]) + "\t" + str(name[pos][7][3:]) + "\t")
    output_lfmm.write(str(locs_lfmm[pos]) + "\t")
    output_lfmm.write(str(inds_lfmm[pos]))
output_lfmm.close()
