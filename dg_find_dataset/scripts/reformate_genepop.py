file1 = str(snakemake.input)
output = str(snakemake.output)

#read data
with open(file1) as file2:
    i = 0
    loci = []
    data = {}
    for line in file2:
        if i == 0:
            title = line.rstrip("\n")
        else:
            line2 = line.rstrip("\n").replace(" ", "\t").replace("\t\t", "\t").replace("<[][]>", "NN").replace("<[", "").replace("]>", ""). replace("][", "").split("\t")
            if len(line2) == 3:
                name = line2[1][7:].replace(".","_")
                loci.append(name)
            elif len(line2) > 3:
                ind = line2[0]
                locus = line2[2:]
                locus2 = []
                for lo in locus:
                    alleles = ""
                    for l in lo:
                        if l == "A":
                            alleles = alleles + "01"
                        elif l == "C":
                            alleles = alleles + "02"
                        elif l == "G":
                            alleles = alleles + "03"
                        elif l == "T":
                            alleles = alleles + "04"
                        else:
                            alleles = alleles + "00"
                    locus2.append(alleles)
                data[ind] = locus2


        i += 1


#output data

out = open(output, "w")
out.write(str(title) + "\n")
for loc in loci[:-1]:
    out.write(str(loc) + ",")
out.write(str(loci[-1]) + "\nPOP")
indis = sorted(data.keys())
for ind in indis:
    out.write("\n" + str(ind) + " ,  ")
    lo = data.get(ind)
    for l in lo[:-1]:
        out.write(str(l) + "\t")
    out.write(str(lo[-1]))
out.close()
