input_lfmm = snakemake.input.lfmm
input_best = snakemake.input.best
input_nlfmm = snakemake.input.nlfmm
input_npop = snakemake.input.npop
output = snakemake.output


#PCA data
def get_pca_data(input_file):
    data_pca = {}
    for file1 in input_file:
        file2 = file1[:file1.find(".lfmm")] + ".pca/" + file1[file1.find("Dg"):file1.find(".lfmm")] + ".tracywidom"
        name = file1[file1.find("Dg"):file1.find("_nf")]
        perc = []
        try:
            l = 0
            for line in open(file2):
                if l > 0:
                    line2 = line.rstrip("\n").replace("\t\t","\t").split("\t")
                    if float(line2[3]) < 0.05:
                        perc.append(float(line2[5]))
                l += 1
            data_pca[name] = perc
        except:
            data_pca[name] = perc
    return(data_pca)


#get K best run snmf
def get_best_data(input_file):
    data_best = {}
    for file1 in input_file:
        k = "ND"
        name = file1[file1.find("Dg"):file1.find(".info")]
        for line in open(file1):
            line2 = line.rstrip("\n").replace('"', '').split("\t")
            if line2[0] == "K":
                k = line2[1]
        data_best[name] = k

    return(data_best)

# Get number of inds and loci per dataset
def get_ind_loc_data(input_file):
    data_il = {}
    for line in open(input_file):
        line2 = line.rstrip("\n").replace('"', '').split("\t")
        name = line2[0] + "_b" + str(line2[1]) + "_c" + str(line2[2]) + "_s" + str(line2[3]) + "_maf" + str(line2[4]) + "_mds" + str(line2[5]) + "_mdi" + str(line2[6])
        data = (line2[7],line2[8])
        data_il[name] = data
    return(data_il)

data_pca = get_pca_data(input_lfmm)
data_best = get_best_data(input_best)
data_npop = get_ind_loc_data(input_npop)
data_nlfmm = get_ind_loc_data(input_nlfmm)

datasets = list(sorted(data_best.keys()))
out = open(output[0], "w")
out.write("batch\tcover\tsnps\tmds\tmdi\t")
out.write("p-loci\tp-inds\tl-loci\tl-inds\t")
out.write("best-K\tPCA-Per")
for data in datasets:
    types = data.split("_")
    batch = str(types[2][1:])
    cover = str(types[3][1:])
    snps = str(types[4][1:])
    mds = str(types[6][3:])
    mdi = str(types[7][3:])
    out.write("\n" + batch + "\t" + cover + "\t" + snps + "\t" + mds + "\t" + mdi)
    npop = data_npop.get(data)
    nlfmm = data_nlfmm.get(data)
    out.write("\t" + str(npop[0]) + "\t" + str(npop[1]) + "\t" + str(nlfmm[0]) + "\t" + str(nlfmm[1]))
    best = data_best.get(data)
    out.write("\t" + str(best))
    pca = data_pca.get(data)
    if len(pca) > 0:
        for p in pca:
            out.write("\t" + str(p))
    else:
        out.write("\t" + "ND")

out.close()
