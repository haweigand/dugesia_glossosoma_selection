import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.mlab import bivariate_normal
import numpy as np
from pandas import DataFrame


#lfmm summary files were generated using conduct_lfmm.Rscript
#pcadapt summary files were generated unsig conduct_pcadapt.Rscript


# functions for the calculations
def open_lfmm_res(folder,file1,qval):
    file2 = folder  + "/" + file1
    loci = []
    info = {}
    l = 0
    for line in open(file2):
        if l > 0:
            line2 = line.rstrip("\n").split("\t")
            if float(line2[3]) < qval:
                loci.append(line2[0])
                info[line2[0]] = [float(line2[1]),float(line2[3])]
        l += 1
    return(loci,info)

def get_loci_names(pca_locs):
    loc_list = []
    for line in open(pca_locs):
        line2 = line.rstrip("\n")
        loc_list.append(line2)
    return(loc_list)

def get_pc_loci(pca,pca_locs,qval):
    l = -1
    sig_loci = []
    info = {}
    loc_list = get_loci_names(pca_locs)
    for line in open(pca):
        if l > -1:
            line2 = line.rstrip("\n").split(" ")
            if float(line2[1]) < qval:
                sig_loci.append(loc_list[l])
                info[loc_list[l]] = ["",line2[1]]
        l += 1
    return(sig_loci,info)

def show_values(pc, fmt="%.2f", **kw):
    pc.update_scalarmappable()
    ax = pc.get_axes()
    maxim = 0
    for p, color, value in zip(pc.get_paths(), pc.get_facecolors(), pc.get_array()):
        x, y = p.vertices[:-2, :].mean(0)
        if np.all(color[:3] > 0.5):
            color = (0.0, 0.0, 0.0)
        else:
            color = (1.0, 1.0, 1.0)
        ax.text(x, y, value, ha="center", va="center", color=color, **kw)

def plot_heatmap(data, colnames, maxi, plot_name, filename):
    df = DataFrame(data, index=colnames, columns=colnames)
    plt.figure(figsize=(20,15))
    plt.title(plot_name)
    plt.yticks(np.arange(0.5, len(colnames), 1), colnames, fontsize=15)
    plt.xticks(np.arange(0.5, len(colnames), 1), colnames, fontsize=15, rotation='vertical')
    c = plt.pcolor(df, edgecolors='k',  linewidths=0.5,cmap='YlOrRd', vmin=0.0, vmax=maxi)
    plt.colorbar(c)
    matplotlib.rcParams.update({'font.size': 11})
    show_values(c, maxi)
    plt.savefig(filename)

def get_all_loci_tests(all_res):
    names = sorted(all_res.keys())
    all_locs = []
    for na in names:
        all_locs = all_locs + all_res.get(na).keys()
    all_locs = sorted(list(set(all_locs)))
    return(all_locs,names)

#alpha threshold value
thresh_lfmm = 0.1
thresh_pcadapt = 0.1

#K + x value (1,2,3)
kval = 1

#files generated using conduct_lfmm.Rscript
file1 = "SigLoc_all.tsv"

#file generated using conduct_pcadapt.Rscript
pca1 = "/home/hannah/ddrad_analyse/Dugesia/small/snake_lfmm_new/pcadapt/pca_b5_s7_K11.txt"

#locus names in the pcadapt analysis
pca_locs = "/home/hannah/ddrad_analyse/Dugesia/small/snake_lfmm_new/lfmm/b5_s7.lfmm_loci"


#load results for different lfmm analyses
all_res = {}
all_res_info = {}
dire = os.getcwd()
fofi = os.listdir(dire + "/K" + str(kval) + "/")
for fo in fofi:
    if fo.endswith("_res"):
        res,res_info = open_lfmm_res("K" + str(kval) + "/" + fo, file1, thresh_lfmm)
        all_res[fo] = res
        all_res_info[fo] = res_info

#load results from the pcadapt analysis
pc1,pc1_info = get_pc_loci(pca1,pca_locs,thresh_pcadapt)
all_res["pcadapt"] = pc1
all_res_info["pcadapt"] = pc1_info

#get dataset and locus names
all_locs,names = get_all_loci_tests(all_res_info)

# bing datasets in format for plotting
data_heat = []
maxi = 0
for x in sorted(all_res.keys()):
    x2 = all_res.get(x)
    overlap = []
    for y in sorted(all_res.keys()):
        y2 = all_res.get(y)
        over = [y3 for y3 in y2 if y3 in x2]
        overlap.append(len(over))
    data_heat.append(overlap)
    if max(overlap) > maxi:
        maxi = max(overlap)

plot_heatmap(data_heat,names,maxi,"summary overview","summary_overview_k" + str(kval) + ".svg")

#print number of loci found by at least on of the methods
all_loci = []
for re in all_res.values():
    for r in re:
        all_loci.append(r)
print("Number of loci: " + str(len(list(set(all_loci)))))


#generate list per locus giving the name of the significant tests
all_res2 = {}
for test,loc_list in all_res.items():
    for lo in loc_list:
        old = all_res2.get(lo,[])
        old.append(test)
        all_res2[lo] = old

output2 = "summary_overview_per_locus_k" + str(kval) + ".tsv"
out2 = open(output2, "w")
out2.write("Locus\t# Sig. Tests\tSig. Tests")
for loc in sorted(all_res2.keys()):
    sig = all_res2.get(loc)
    out2.write("\n" + loc + "\t" + str(len(sig)) + "\t" + ";".join(sorted(sig)))
out2.close()


#generate tables including q-values for lfmm and pcadapt and z-scores for lfmm per locus
output3 = "summary_results_per_locus"
out4 = open(output3 + "_qvalues.tsv", "w")
out5 = open(output3 + "_zscores.tsv", "w")
out4.write("Locus\t" + "\t".join(names))
out5.write("Locus\t" + "\t".join(names[:-1]))
for loc in all_locs:
        vas1 = [str(all_res_info.get(test).get(loc,["",""])[1]) for test in names]
        vas2 = [str(all_res_info.get(test).get(loc,["",""])[0]) for test in names]
        out4.write("\n" + loc + "\t" + "\t".join(vas1))
        out5.write("\n" + loc+ "\t" + "\t".join(vas2))
out4.close()
out5.close()
